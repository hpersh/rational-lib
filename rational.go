package rational

import "fmt"

type Rational struct {
	numer, denom int
};

func gcd(a, b int) int {
	if a < 0 {
		a = -a
	}
	if b < 0 {
		b = -b
	}
	if b > a {
		a, b = b, a
	}
	for b != 0 {
		a, b = b, a % b
	}
	return a
}

func lcm(a, b int) int {
	if a < 0 {
		a = -a
	}
	if b < 0 {
		b = -b
	}
	return (a * b) / gcd(a, b)
}

func RationalNew(numer, denom int) *Rational {
	negf := false
	if numer < 0 {
		negf = !negf
		numer = -numer
	}
	if denom < 0 {
		negf = !negf
		denom = -denom
	}
	d := gcd(numer, denom)
	if negf {
		numer = -numer
	}
	return &Rational{numer: numer / d, denom: denom / d}
}

func RationalNew1(i int) *Rational {
	return &Rational{numer: i, denom: 1}	
}

func (q *Rational) Negate() *Rational {
	return RationalNew(-q.numer, q.denom)
}

func (q *Rational) Recip() *Rational {
	return RationalNew(q.denom, q.numer)
}

func (q *Rational) Add(r *Rational) *Rational {
	denom := lcm(q.denom, r.denom)
	return RationalNew((denom / q.denom) * q.numer + (denom / r.denom) * r.numer, denom)
}

func (q *Rational) Sub(r *Rational) *Rational {
	denom := lcm(q.denom, r.denom)
	return RationalNew((denom / q.denom) * q.numer - (denom / r.denom) * r.numer, denom)
}

func (q *Rational) Mul(r *Rational) *Rational {
	return RationalNew(q.numer * r.numer, q.denom * r.denom)
}

func (q *Rational) Div(r *Rational) *Rational {
	return RationalNew(q.numer * r.denom, q.denom * r.numer)
}

func (q *Rational) String() string {
	negf := false
	if q.numer < 0 {
		negf = !negf
		q.numer = -q.numer
	}
	if q.denom < 0 {
		negf = !negf
		q.denom = -q.denom
	}
	result := ""
	if negf {
		result += "-"
	}
	result += fmt.Sprintf("%v", q.numer)
	if q.denom != 1 {
		result += fmt.Sprintf("/%v", q.denom)
	}
	return result
}
