package main

import (
	"fmt"
	"gitlab.com/hpersh/rational"
)

func main() {
	fmt.Printf("%s\n", rational.RationalNew(7, 14))
	fmt.Printf("%s\n", rational.RationalNew(7, 14).Add(rational.RationalNew(1, 3)))
	fmt.Printf("%s\n", rational.RationalNew(7, -14).Add(rational.RationalNew(1, 3)))
	fmt.Printf("%s\n", rational.RationalNew(7, 14).Mul(rational.RationalNew(1,3 )))
}
